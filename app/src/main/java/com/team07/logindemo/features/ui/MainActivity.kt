package com.team07.logindemo.features.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.team07.logindemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        val bundle: Bundle? = intent.extras
        val userName = bundle?.getString("UserName")
        mBinding.txtUserName.text = "Hello".plus(" ").plus(userName)


    }
}