package com.team07.logindemo.features.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import com.team07.logindemo.R
import com.team07.logindemo.databinding.ActivityLoginBinding
import com.team07.logindemo.utils.Validator
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

class LoginActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityLoginBinding

    private val userName = MutableStateFlow("")
    private val password = MutableStateFlow("")

    private var userNameErrorMessage: String? = null
    private var passwordErrorMessage: String? = null

    private val isLoginEnable = combine(
        userName,
        password
    ) { username, password ->
        mBinding.txtUserNameError.text = ""
        mBinding.txtPasswordError.text = ""
        val isUserNameValid = Validator.isUserNameValid(username)
        val isPasswordValid = Validator.isPasswordValid(password)
        userNameErrorMessage = when {
            isUserNameValid.not() -> getString(R.string.user_name_error_label)
            else -> null
        }
        passwordErrorMessage = when {
            isPasswordValid.not() -> getString(R.string.password_error_label)
            else -> null
        }
        userNameErrorMessage?.let {
            if (mBinding.edtUserName.text.isNullOrEmpty()) {
                mBinding.txtUserNameError.visibility = View.GONE
            } else {
                mBinding.txtUserNameError.visibility = View.VISIBLE
                mBinding.txtUserNameError.text = it
            }


        }
        passwordErrorMessage?.let {
            if (mBinding.edtPassword.text.isNullOrEmpty()) {
                mBinding.txtPasswordError.visibility = View.GONE
            } else {
                mBinding.txtPasswordError.visibility = View.VISIBLE
                mBinding.txtPasswordError.text = it
            }

        }
        isUserNameValid and isPasswordValid
    }

    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        with(mBinding) {
            edtUserName.doOnTextChanged { text, _, _, _ ->
                userName.value = text.toString()
            }
            edtPassword.doOnTextChanged { text, _, _, _ ->
                password.value = text.toString()
            }
        }
        mBinding.mButtonLogin.setOnClickListener {
            navigateToMainScreen()
        }

        lifecycleScope.launch {
            isLoginEnable.collectLatest {
                mBinding.mButtonLogin.isEnabled = it
            }

        }
    }

    private fun navigateToMainScreen() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("UserName", userName.value)
        startActivity(intent)
    }
}