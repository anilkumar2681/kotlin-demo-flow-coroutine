package com.team07.logindemo.utils

import java.util.regex.Pattern

class Validator {

    companion object {
        fun isUserNameValid(userName: String): Boolean {
            //User name should contain only numbers 0-9, and characters a-z, A-Z
            val matcher: Pattern = Pattern.compile("^[a-zA-Z0-9]+$")
            return userName.isNotBlank() && matcher.matcher(userName).matches()
        }

        fun isPasswordValid(password: String): Boolean {
            var valid = true

            // Password policy check
            // Password should be minimum minimum 5 characters long
            if (password.length < 5) {
                valid = false
            }
            // Password should contain at least one number
            var exp = ".*[0-9].*"
            var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
            var matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                valid = false
            }
            // Password should contain at least one capital letter
            exp = ".*[A-Z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                valid = false
            }
            // Password should contain at least one small letter
            exp = ".*[a-z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                valid = false
            }
            // Password should contain at least one special character
            // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
            exp = ".*[!@\\_\\?].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                valid = false
            }
            return valid
        }
    }

}